


too_many_abms.nodes = {}

local n = too_many_abms.nodes

-- inbuilt
n.rotting_log = "too_many_abms:rotting_log"

n.water_source = "mcl_core:water_source"
n.water_flowing = "mcl_core:water_flowing"
n.oak_log = "mcl_core:tree"
n.mushroom_brown = "mcl_mushrooms:mushroom_brown"
n.mushroom_red = "mcl_mushrooms:mushroom_red"
n.dirt = "mcl_core:dirt"
n.stone = "mcl_core:stone"
n.cobble = "mcl_core:cobble"
n.leaves = "mcl_core:leaves"
-- n.water_source = "mcl_core:water_source"
-- n.water_source = "mcl_core:water_source"
-- n.water_source = "mcl_core:water_source"
