local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)

if minetest.get_modpath("mcl_core") then
  dofile(mod_path .. DIR_DELIM .. "compatibility" .. DIR_DELIM .. "mcl.lua")
elseif minetest.get_modpath("default") then
  dofile(mod_path .. DIR_DELIM .. "compatibility" .. DIR_DELIM .. "default.lua")
end