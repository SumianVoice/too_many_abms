
local n = too_many_abms.nodes
local tmult = too_many_abms.time_multiplier -- debug


minetest.register_abm({
  nodenames = {n.oak_log},
  neighbors = {n.water_source, n.water_flowing},
  interval = 10.0 * tmult,
  chance = 50,
  action = function(pos, node, active_object_count, active_object_count_wider)
    if too_many_abms.is_touching_sides(pos, {n.water_source})
    or too_many_abms.is_touching_sides(pos, {n.water_flowing}) then
      local param2 = node.param2
      minetest.set_node(pos, {name = n.rotting_log, param2 = param2})
    end
  end
})

minetest.register_abm({
  nodenames = {n.rotting_log},
  neighbors = {"air"},
  interval = 10.0 * tmult,
  chance = 50,
  action = function(pos, node, active_object_count, active_object_count_wider)
    if minetest.get_node(vector.offset(pos, 0, 1, 0)).name == "air"
    and minetest.get_node_light(vector.offset(pos, 0, 1, 0)) < 15 then
      local r = math.random()
      if r < 0.45 then
        minetest.set_node(vector.offset(pos, 0, 1, 0), {name = n.mushroom_brown})
      elseif r < 0.9 then
        minetest.set_node(vector.offset(pos, 0, 1, 0), {name = n.mushroom_red})
      else
        minetest.set_node(pos, {name = n.dirt})
      end
    end
  end
})