
local n = too_many_abms.nodes
local tmult = too_many_abms.time_multiplier -- debug

local adjacent = {
  vector.new(0, 1, 0),
  vector.new(1, 0, 0),
  vector.new(0, 0, 1),
  vector.new(-1, 0, 0),
  vector.new(0, 0, -1),
}

local function leaf_decay(pos, count)
  if count > 4 then return end
  if minetest.get_node(pos).name == n.leaves then
    minetest.dig_node(pos)
  end
  for _, p in pairs(adjacent) do
    local v = vector.add(pos, p)
    if minetest.get_node(v).name == n.leaves
    and too_many_abms.is_touching_sides(v, {n.oak_log}) == 0 then
      minetest.after(0.5, function(pos)
        leaf_decay(v, count+1)
      end, v)
    end
  end
end

local function tree_fell(pos)
  minetest.after(0.3, function(pos)
    if minetest.get_node(pos).name == n.oak_log
    and too_many_abms.is_touching_sides(pos, {n.oak_log}) <= 2 then
      minetest.dig_node(pos)
      tree_fell(vector.offset(pos, 0, 1, 0))
      leaf_decay(pos, 1)
    end
  end, pos)
end

minetest.register_abm({
  nodenames = {n.oak_log},
  neighbors = {"air"},
  interval = 10.0 * tmult,
  chance = 5,
  action = function(pos, node, active_object_count, active_object_count_wider)
    if minetest.get_node(vector.offset(pos, 0, -1, 0)).name == "air" then
      tree_fell(pos)
    end
  end
})

minetest.register_abm({
  nodenames = {n.leaves},
  neighbors = {"air"},
  interval = 10.0 * tmult,
  chance = 50,
  action = function(pos, node, active_object_count, active_object_count_wider)
    if minetest.get_node(vector.offset(pos, 0, -1, 0)).name == "air"
    and not too_many_abms.is_touching_sides(pos, {n.leaves})
    and not too_many_abms.is_touching_sides(pos, {n.oak_log}) then
      minetest.dig_node(pos)
    end
  end
})