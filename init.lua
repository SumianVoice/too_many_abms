local mod_name = minetest.get_current_modname()
local mod_path = minetest.get_modpath(mod_name)
local S = minetest.get_translator(mod_name)

too_many_abms = {}
too_many_abms.nodes = {}
too_many_abms.time_multiplier = 1

dofile(mod_path .. DIR_DELIM .. "system.lua")
dofile(mod_path .. DIR_DELIM .. "items.lua")
dofile(mod_path .. DIR_DELIM .. "compatibility" .. DIR_DELIM .. "compatibility.lua")
dofile(mod_path .. DIR_DELIM .. "abm" .. DIR_DELIM .. "abm.lua")