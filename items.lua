
if not mcl_util then
  mcl_util = {}
  mcl_util.rotate_axis = function() return nil end
end
if not mcl_sounds then
  mcl_sounds = {}
  mcl_sounds.node_sound_wood_defaults = function() return nil end
end

minetest.register_node("too_many_abms:rotting_log", {
  description = "A log that is rotting.",
  -- _doc_items_longdesc = longdesc,
  _doc_items_hidden = false,
  tiles = {
    "default_tree_top.png^tmabms_rotting_log_overlay.png",
    "default_tree_top.png^tmabms_rotting_log_overlay.png",
    "default_tree.png^tmabms_rotting_log_overlay.png"},
  paramtype2 = "facedir",
  on_place = mcl_util.rotate_axis,
  stack_max = 64,
  groups = {handy=1,axey=1, tree=1, flammable=0, building_block=1, material_wood=1,},
  sounds = mcl_sounds.node_sound_wood_defaults(),
  on_rotate = on_rotate,
  _mcl_blast_resistance = 2,
  _mcl_hardness = 2,
})