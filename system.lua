
too_many_abms.sides = {
  {x= 1, y=0, z= 0},
  {x=-1, y=0, z= 0},
  {x= 0, y=0, z= 1},
  {x= 0, y=0, z=-1},
}
function too_many_abms.is_touching_sides(pos, nodenames)
  local num = 0
  for _, nn in pairs(nodenames) do
    for _, offset in pairs(too_many_abms.sides) do
      if minetest.get_node(vector.add(pos, offset)).name == nn then
        num = num + 1
      end
    end
  end
  return num
end